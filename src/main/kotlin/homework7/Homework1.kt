package homework7

import java.lang.Exception

fun main() {
    println("Введите логин:")
    val login: String = readln()
    println("Введите пароль:")
    val password: String = readln()
    println("Подтведите пароль:")
    val confirmation: String = readln()
    register(login, password, confirmation)
}

//write your function and Exception-classes here
fun register(login: String, password: String, confirmation: String): User {
    if (login.count() !in 1..20) {
        throw WrongLoginException("Некорректная длина логина. Логин должен быть менее 20 символов")
    } else if (password.count() < 10) {
        throw WrongPasswordException("Пароль слишком короткий. Пароль должен быть от 10 символов")
    } else if (password !== confirmation) {
        throw WrongPasswordException("Пароль и подтверждение не совпадают")
    } else return User(login, password)
}

class WrongLoginException(message: String) : Exception(message)

class WrongPasswordException(message: String) : Exception(message)


class User(val login: String, val password: String)
