package homework4

fun main() {
    val marks = arrayOf(3, 4, 5, 2, 3, 5, 5, 2, 4, 5, 2, 4, 5, 3, 4, 3, 3, 4, 4, 5)
    //write your code here
    var bestPupils = 0
    var goodPupils = 0
    var normPupils = 0
    var badPupils = 0
    for (item in marks) {
        when (item) {
            5 -> bestPupils++
            4 -> goodPupils++
            3 -> normPupils++
            2 -> badPupils++
        }
    }
    println("Отличников - " + bestPupils.toDouble() / marks.size * 100 + "%")
    println("Хорошистов - " + goodPupils.toDouble() / marks.size * 100 + "%")
    println("Троечников - " + normPupils.toDouble() / marks.size * 100 + "%")
    println("Двоечников - " + badPupils.toDouble() / marks.size * 100 + "%")
}