package homework4

fun main() {
    val myArray = arrayOf(1, 25, 10, 6, 22, 17, 8, 14, 23)
    var joeCoins = 0
    var teamCoins = 0
    for (item in myArray) {
        if (item % 2 == 0) {
            joeCoins++
        } else teamCoins++
    }
    println("Джо получил $joeCoins монет, команда получила $teamCoins монет")

}
