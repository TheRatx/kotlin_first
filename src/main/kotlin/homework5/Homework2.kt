package homework5

fun main() {
    val digit = readLine()!!.toInt()
    //call written function here
    println(flipNumber(digit))
}

//write your function here
fun flipNumber(number: Int): Int {
    var num = number
    var flipped = 0

    while (num != 0) {
        val digit = num % 10
        flipped = flipped * 10 + digit
        num /= 10
    }
    return flipped
}