package homework5

//write your classes here
fun main() {
    val backet: Array<String> = arrayOf("курица", "Картошка", "морошка", "лукошко","мясо","трава")
    val zoo: Array<Animal> = arrayOf(
        Lion("Симба", 120.0, 150),
        Tiger("Шерхан", 100.0, 140),
        Hippo("Булли", 250.0, 500),
        Wolf("Серый", 100.0, 80),
        Giraffe("Шпала", 300.0, 140),
        Elephant("Дамбо", 270.8, 200),
        Chimpanzee("Чичи", 134.4, 20),
        Gorilla("Бруха", 180.5, 140)
    )
    startEat(backet, zoo)
}

fun startEat(food: Array<String>, animals: Array<Animal>) {
    for (product in food) {
        for (animal in animals) {
            animal.eat(product)
        }
    }
}

abstract class Animal(open val name: String, open val height: Double, open val weight: Int) {
    abstract val foodPreferences: Array<String>
    abstract var hunger: Int

    fun eat(food: String) {
        val lowFood = food.lowercase()
        if (hunger < 10) if (foodPreferences.contains(lowFood)) {
            hunger++
            println("$name съел $lowFood")
        } else println("$name не ест $lowFood")
        else println("$name не голоден")
    }

}

class Lion(override val name: String, override val height: Double, override val weight: Int) :
    Animal(name, height, weight) {
    override val foodPreferences: Array<String> = arrayOf("антилопа", "бизон")
    override var hunger: Int = 0

}


class Tiger(override val name: String, override val height: Double, override val weight: Int) :
    Animal(name, height, weight) {
    override val foodPreferences: Array<String> = arrayOf("антилопа", "лама")
    override var hunger: Int = 0
}


class Hippo(override val name: String, override val height: Double, override val weight: Int) :
    Animal(name, height, weight) {
    override val foodPreferences: Array<String> = arrayOf("трава", "ананас")
    override var hunger: Int = 0

}

class Wolf(override val name: String, override val height: Double, override val weight: Int) :
    Animal(name, height, weight) {
    override val foodPreferences: Array<String> = arrayOf("кролик", "заяц")
    override var hunger: Int = 0

}

class Giraffe(override val name: String, override var height: Double, override var weight: Int) :
    Animal(name, height, weight) {
    override val foodPreferences: Array<String> = arrayOf("манго", "яблоко")
    override var hunger: Int = 9

}

class Elephant(override val name: String, override var height: Double, override var weight: Int) :
    Animal(name, height, weight) {
    override val foodPreferences: Array<String> = arrayOf("фрукты", "кокос")
    override var hunger: Int = 10

}

class Chimpanzee(override val name: String, override var height: Double, override var weight: Int) :
    Animal(name, height, weight) {
    override val foodPreferences: Array<String> = arrayOf("мясо", "рыба")
    override var hunger: Int = 0

}

class Gorilla(override val name: String, override var height: Double, override var weight: Int) :
    Animal(name, height, weight) {
    override val foodPreferences: Array<String> = arrayOf("ягоды", "фрукты")
    override var hunger: Int = 0


}