package homework2

fun main() {
    val volumeLimonade = 18500
    val volumePina: Short = 200
    val volumeWisky: Byte = 50
    val volumeFresh = 3000000000L
    val volumeCola = 0.5f
    val volumeEl = 0.666666667
    val authorDrink = "Что-то авторское!"

    println("Заказ - '$volumeLimonade мл лимонада' готов!")
    println("Заказ - '$volumePina мл пина колады' готов!")
    println("Заказ - '$volumeWisky мл виски' готов!")
    println("Заказ - '$volumeFresh капель фреша' готов!")
    println("Заказ - '$volumeCola литра колы' готов!")
    println("Заказ - '$volumeEl литра эля' готов!")
    println("Заказ - '$authorDrink' готов!")
}